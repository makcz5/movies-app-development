import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as MoviesListActions from './store/movies-list.actions';
import { AppState } from './store/movies-list.state.model';
import { Movie } from '../movie.model';

@Component({
    selector: 'app-movies-list',
    templateUrl: './movies-list.component.html',
    styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {

    movies: Observable<Movie[]>;
    query = "";
    errorMessage = null;
    loading = false;

    constructor(private store: Store <AppState>) { 
            this.movies = this.store.select('movies');
        }

    fetchMovies(){
        if(this.query.length < 3){
            this.errorMessage = "Please enter more than 2 characters";
        } else {
            this.store.dispatch(new MoviesListActions.startSearch({query: this.query, loading: this.loading}));
            this.errorMessage = null;
        }
    };

    ngOnInit() {
    }
}       