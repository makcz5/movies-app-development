import * as MoviesListActions from './movies-list.actions';
import { Movie } from '../../movie.model';

export type Action = MoviesListActions.All;

const initialState: Array<Movie> = [];

export function moviesListReducer(state: Array<Movie> = initialState, action: Action) {
  switch (action.type) {
    case MoviesListActions.ADD_MOVIE:          
            return action.payload; 
    default:
        return state;
  }
}