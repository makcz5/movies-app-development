import { Movie } from '../../movie.model';

export interface AppState {
    readonly movies: Movie[];
}