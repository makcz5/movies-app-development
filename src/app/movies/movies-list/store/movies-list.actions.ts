import { Action } from '@ngrx/store';
import { Movie } from '../../movie.model';

export const START_SEARCH = 'START_SEARCH';
export const ADD_MOVIE = 'ADD_MOVIE';

export class startSearch implements Action {
    readonly type = START_SEARCH;

    constructor(public payload: {query: string, loading: boolean}) { }
    }

export class addMovie implements Action {
    readonly type = ADD_MOVIE;

    constructor(public payload: Array<Movie>) { }
    }

export type All = startSearch | addMovie;