import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { switchMap, map} from 'rxjs/operators';

import * as MoviesListActions from './movies-list.actions';

export type Action = MoviesListActions.All;

@Injectable()
export class MoviesListEffects {

  @Effect()
  fetchMovies = this.actions$.pipe(
    ofType(MoviesListActions.START_SEARCH),
    switchMap((data: MoviesListActions.startSearch) => {
        
        return this.http.get('http://www.omdbapi.com/?apikey=f79aeba3&s=' +
        data.payload.query
        );
    }),
    map(data => {
        const moviesArray = [];

        for (const key in data){
            moviesArray.push(data[key])
        }
        return new MoviesListActions.addMovie(moviesArray[0]);
    })
  );
  constructor(private actions$: Actions, private http: HttpClient) {}
}