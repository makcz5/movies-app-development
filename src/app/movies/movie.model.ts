export class Movie {
    public Title: string;
    public Poster: string;
    public Year: string;
    public Type: string
    public imdbID: string;

    constructor(Title: string, Poster: string, Year: string, Type: string, imdbID: string) {
        this.Title = Title;
        this.Poster = Poster;
        this.Year = Year;
        this.Type = Type;
        this.imdbID = imdbID;
    }
}