import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-detailed',
  templateUrl: './movie-detailed.component.html',
  styleUrls: ['./movie-detailed.component.scss']
})
export class MovieDetailedComponent implements OnInit {

    constructor(private http: HttpClient, private route: ActivatedRoute) { }

    movie = [];
    isFetching = false;

    ngOnInit() {
        this.fetchMovie();
    }

    private fetchMovie() {
        this.isFetching = true;
        const movieId = this.route.snapshot.params['id'];
        const url = 'http://www.omdbapi.com/?apikey=f79aeba3&i=' + movieId;

        this.http
            .get(url)
            .pipe(
                map(responseData => {
                    return responseData;
                })
            )
            .subscribe(responseData => {
                this.isFetching = false;
                this.movie.push(responseData);
            });
    }
}
