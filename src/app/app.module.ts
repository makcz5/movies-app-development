import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MoviesListEffects } from './movies/movies-list/store/movies-list.effects';
import { environment } from '../environments/environment'; // Angular CLI environment
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import { MovieDetailedComponent } from './movies/movie-detailed/movie-detailed.component';
import { AppRoutingModule } from './app-routing.module';
import { moviesListReducer } from './movies/movies-list/store/movies-list.reducer';

const appRoutes: Routes = [
    {path: 'movie:id', component: MovieDetailedComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    MoviesListComponent,
    MovieDetailedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({movies: moviesListReducer}),
    EffectsModule.forRoot([MoviesListEffects]),
    StoreDevtoolsModule.instrument({
        maxAge: 25, 
        logOnly: environment.production
      })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }